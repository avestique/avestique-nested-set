<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file NestedSet.php
 */

include_once realpath(__DIR__) . '/NestedSetEvent.php';
include_once realpath(__DIR__) . '/Helper/Data.php';
include_once realpath(__DIR__) . '/Model/Crud.php';
include_once realpath(__DIR__) . '/Model/NestedSetAbstract.php';

class NestedSet extends Avestique_NestedSetAbstract
{
    const PRIMARY_INTEGEGER_MIN_SIZE = 11;
    const INTEGEGER_MIN_SIZE = 11;
    const VARCHAR_MIN_SIZE = 256;
    const PARENT_DELIMITER = '/';

    const PLACE_BEFORE = -1;
    const PLACE_AFTER  = 1;
    const PLACE_END    = 0;


    /**
     *
     * @param string $adapter 'Yii' for a while
     */
    function __construct($options = array())
    {
        $this->setAdapterOptions($options);

        $this->crudModel = new Avestique_Crud();

        $this->setOptions($options);

        $this->init();

        $this->afterInit();
    }

    static public function getListEvents()
    {
        return Avestique_Event::getListEvents();
    }

    /**
     * Create new node.
     *
     * @param array $data
     * @param int $parentID If parentID = 0 its a root directory.
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */
    public function createNode($data, $parentID = 0, $idNeighbor = NULL, $position = self::PLACE_END)
    {
        $this->getCRUDModel()->createNode($data, $parentID, $idNeighbor, $position);

        return $this;
    }

    /**
     * Remove node
     *
     * @param $idNode
     */
    public function removeNode($idNode)
    {
        $this->getCRUDModel()->removeNode($idNode);

        return $this;
    }

    /**
     * Move node.
     *
     * @param int $currentID
     * @param int $newParentID move to new parent
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */
    public function moveNode($currentID, $newParentID, $idNeighbor = NULL, $position = self::PLACE_END)
    {
        $this->getCRUDModel()->moveNode($currentID, $newParentID, $idNeighbor, $position);

        return $this;
    }

    /**
     * Copy node.
     * @todo copy all branch
     * @param array $node
     * @param int $newParentID move to new parent
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */
    public function copyNode($node, $parentID, $idNeighbor = NULL, $position = self::PLACE_END)
    {

    }

    /**
     * Edit node
     *
     * @param $currentID
     * @param array $data
     *
     */
    public function editNode($currentID, $data)
    {
        return $this->getCRUDModel()->editNode($currentID, $data);
    }

    /**
     * Find node or nodes
     *
     * @param mixed $idNode array|integer
     */
    public function findNode($idNode)
    {
        return $this->getCRUDModel()->findNode($idNode);
    }

    /**
     * Find All Children Nodes
     *
     * @param $idParentNode
     * @param bool $asTree show as a tree array
     */
    public function findChildrenNodes($idParentNode, $asTree = false)
    {
        return $this->getCRUDModel()->findChildrenNodes($idParentNode, $asTree);
    }

    public function findParents($idNode, $asTree = false)
    {
        return $this->getCRUDModel()->findParents($idNode, $asTree = false);
    }

    public function findLeftNeighbor($idNode)
    {
        return $this->getCRUDModel()->findLeftNeighbor($idNode);
    }

    public function findLeftNeighbors($idNode)
    {
        return $this->getCRUDModel()->findLeftNeighbors($idNode);
    }

    public function findRightNeighbor($idNode)
    {
        return $this->getCRUDModel()->findRightNeighbor($idNode);
    }

    public function findRightNeighbors($idNode)
    {
        return $this->getCRUDModel()->findRightNeighbors($idNode);
    }

    public function findNeighbors($idNode)
    {
        return $this->getCRUDModel()->findNeighbors($idNode);
    }


    public function getRootCategory()
    {
        return $this->getCRUDModel()->getRootCategory();
    }
}