<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Event.php
 */

interface NestedsetEventInterface
{
    public function run($event);
}

class Avestique_Event
{
    /**
     * Registry collection
     *
     * @var array
     */
    static private $_events = array();

    /**
     * Unregister a variable from register by key
     *
     * @param string $key
     */
    public static function unregister($key)
    {
        if (isset(self::$_events[$key])) {
            if (is_object(self::$_events[$key]) && (method_exists(self::$_events[$key], '__destruct'))) {
                self::$_events[$key]->__destruct();
            }
            unset(self::$_events[$key]);
        }
    }

    /**
     * Retrieve a value from registry by a key
     *
     * @param string $key
     * @return mixed
     */
    public static function registry($key)
    {
        if (isset(self::$_events[$key])) {
            return self::$_events[$key];
        }
        return null;
    }

    /**
     * Register a new variable
     *
     * @param string $key
     * @param mixed $value
     * @param bool $graceful
     */
    public static function register($key, $value)
    {
        if ($value instanceof NestedsetEventInterface)
        {
            self::$_events[$key][] = $value;
        }
    }

    /**
     * Throw Exception
     *
     * @param string $message
     * @param string $messageStorage
     */
    public static function throwException($message, $messageStorage = null)
    {
        throw new Exception($message);
    }

    public static function raise($eventName, $params)
    {
        $success = true;

        if (self::registry($eventName))
        {
            foreach(self::registry($eventName) as $event)
            {
                if ($event instanceof NestedsetEventInterface)
                {
                    $success = $success && $event->run($params);
                }
            }
        }

        return $success;
    }

    static public function getListEvents()
    {
        return array(
            '=================================== ON MOVE NODE EVENTS =======================================',
            'before_move'   => array("branch" => "Array of nodes ID in the branch", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_move'    => array("branch" => "Array of node array of the branch", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),

            '=================================== ON REMOVE NODE EVENTS =======================================',
            'before_remove' => array("nodes_to_remove" => "Array of nodes ID to remove", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_remove'  => array("nodes_to_remove" => "Array of nodes ID to remove", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),

            '=================================== ON UPDATE NODE EVENTS =======================================',
            'before_update' => array("before_update" => "Array custom data to save", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_update'  => array("after_update" => "Array custom data to save", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),

            '=================================== ON CREATE NODE EVENTS =======================================',
            'before_offset_node_right_create' => array("neighborNode" => "Neighbor node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_offset_node_right_create'  => array("neighborNode" => "Neighbor node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'before_offset_node_left_create'  => array("neighborNode" => "Neighbor node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_offset_node_left_create'   => array("neighborNode" => "Neighbor node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'before_offset_node_end_create'   => array("parent_node" => "Parent node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_offset_node_end_create'    => array("parent_node" => "Parent node data as array", "event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'before_create'                   => array("event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
            'after_create'                    => array("event" => "pointer to \$this (instance of Avestique_CRUD_Actions_Abstract)"),
        );
    }
}