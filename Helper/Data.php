<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Data.php
 */

include_once realpath(__DIR__) . '/Abstract.php';

class Avestique_Helper extends Avestique_Helper_Abstract
{
    /**
     * Check table types
     * use constant from NestedSet
     *
     * @static
     * @param $fields
     * @throws Exception
     */
    static public function checkTableFields($fields)
    {
        $custom_required_fields = Avestique_Helper::registry('custom_required_fields');

        $_requiredFields = array(
            'id_category' => array('type' => 'integer', 'allow_null' => false, 'default' => false, 'size' => NestedSet::PRIMARY_INTEGEGER_MIN_SIZE, 'primary' => true),
            'name'        => array('type' => 'string',  'allow_null' => false, 'default' => false, 'size' => NestedSet::VARCHAR_MIN_SIZE,   'primary' => false),
            'alias'       => array('type' => 'string',  'allow_null' => false, 'default' => false, 'size' => NestedSet::VARCHAR_MIN_SIZE,   'primary' => false),
            'level'       => array('type' => 'integer', 'allow_null' => false, 'default' => true,    'size' => NestedSet::INTEGEGER_MIN_SIZE, 'primary' => false),
            'path'        => array('type' => 'string',  'allow_null' => true,  'default' => true,    'size' => NestedSet::VARCHAR_MIN_SIZE,   'primary' => false),
            'left'        => array('type' => 'integer', 'allow_null' => false, 'default' => true,    'size' => NestedSet::INTEGEGER_MIN_SIZE, 'primary' => false),
            'right'       => array('type' => 'integer', 'allow_null' => false, 'default' => true,    'size' => NestedSet::INTEGEGER_MIN_SIZE, 'primary' => false),
            'parent_id'   => array('type' => 'integer', 'allow_null' => false, 'default' => true,    'size' => NestedSet::INTEGEGER_MIN_SIZE, 'primary' => false)
        );

        if ($custom_required_fields)
        {
            $errorsMSG = '';

            foreach($custom_required_fields as $systemField => $customField)
            {
                if (isset($_requiredFields[$systemField]) && isset($fields[$customField]))
                {
                    $customFieldProperty = $fields[$customField];

                    if ($customFieldProperty['primary'] !== $fields[$customField]['primary'])
                        $errorsMSG .= " {$customField} should " . ($fields[$customField]['primary'] ? '' : 'not') . " has primary value. ";

                    if ($customFieldProperty['allow_null'] !== $fields[$customField]['allow_null'])
                        $errorsMSG .= " {$customField} cannot be null allowed. ";

                    if ($customFieldProperty['default'] !== $fields[$customField]['default'])
                        $errorsMSG .= " {$customField} should " . ($fields[$customField]['default'] ? '' : 'not') . " has default value. ";

                    if ($customFieldProperty['type'] !== $fields[$customField]['type'])
                        $errorsMSG .= " {$customField} should has {$fields[$customField]['type']} type. ";
                    else if ( $customFieldProperty['size'] < $fields[$customField]['size'] )
                        $errorsMSG .= " {$customField} should has {$fields[$customField]['size']} length at least. ";
                }
            }

            if ($errorsMSG)
                throw new Exception('Table fields has errors: ' . $errorsMSG);
        }
    }

    static public function useSystemData($data)
    {
        $custom_required_fields = Avestique_Helper::registry('custom_required_fields');

        $new_data = array();

        foreach($data as $field => $value)
        {
            $new_data[$field] = $value;

            foreach($custom_required_fields as $sysField => $customField)
            {
                if ($customField == $field)
                {
                    unset($new_data[$field]);
                    $new_data[$sysField] = $value;
                }
            }
        }

        return $new_data;
    }

    static public function useCustomData($data)
    {
        $custom_required_fields = Avestique_Helper::registry('custom_required_fields');

        $new_data = array();

        foreach($data as $field => $value)
        {
            $new_data[$field] = $value;

            foreach($custom_required_fields as $sysField => $customField)
            {
                if ($sysField == $field)
                {
                    unset($new_data[$field]);
                    $new_data[$customField] = $value;
                }
            }
        }

        return $new_data;
    }
}