<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Abstract.php
 */

abstract class Avestique_CRUD_Actions_Abstract
{
    protected $adapter = NULL;

    protected $args = array();

    public $success = true;

    public function init()
    {
        if (!$this->adapter->av_GetCount())
        {
            $data = array(
                'name'  => 'root',
                'alias' => 'root',
                'level' => 0,
                'left'  => 1,
                'right' => 2
            );

            if (!$this->adapter->av_createNode(Avestique_Helper::useCustomData($data)))
            {
                throw new Exception('Root directory creation error');
            }
        }
    }

    function __construct()
    {
        $params = func_get_args();

        if (func_num_args() > 1)
            if ($params[0] instanceof Avestique_Crud_Adapter_Interface)
                $this->adapter = $params[0];

        if (!$this->adapter)
            throw new Exception("Adapter wasn't found as first parameter in construct. ");

        unset($params[0]);

        reset($params);

        $this->args = array_values($params);

        $this->init();

        $this->run();
    }

    public function getResult()
    {
        return $this->success;
    }

    public function getParams()
    {
        return $this->args;
    }

    public function getRootNode()
    {
        $attributes = array(
            'parent_id' => 0,
            'left'      => 1,
            'alias'     => 'root'
        );

        $node = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData($attributes));

        if (count($node) > 1 || !$node)
        {
            throw new Exception("Your table has several roots or hasn't it at all...");
        }

        return current($node);

    }
}