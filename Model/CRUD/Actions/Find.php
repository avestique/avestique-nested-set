<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Find.php
 */

class Avestique_Find_Node extends Avestique_CRUD_Actions_Abstract
{
    protected $idCategory = NULL;

    protected $tree = false;

    protected $nodes = array();

    const FIND_LEFT = -1;
    const FIND_RIGHT = 1;
    const FIND_ALL   = 0;

    protected $leftKey = '';

    public function init()
    {
        parent::init();

        $args = $this->getParams();

        $this->idCategory = (int) $args[0] || is_array($args[0]) ? $args[0] : NULL;
        $this->tree       = $args[1] ? true : false;
    }

    public function run()
    {
        if ($nodes = $this->adapter->av_findNodesByID($this->idCategory))
        {
            foreach($nodes as $node)
                $this->nodes[] = Avestique_Helper::useSystemData($node);
        }
        else
        {
            $this->success = false;
            Avestique_Helper::registerMsg('Cannot find node ' . $this->idCategory . '.');
        }
    }

    static public function getTree($cacheTime = 0)
    {
        $select = CategoryHelper::getModel()->cache($cacheTime)->findAll(array('order' => '`root`,`left`'));


    }

    protected function getHierarchy($tree)
    {
        $levels = array();

        foreach($tree as $category)
        {
            $_category = Avestique_Helper::useSystemData($category);

            $level = (int) $_category['level'];

            if (!isset($levels[$level]))
                $levels[$level] = array();

            $levels[$_category['level']][$_category['id_category']] = $category;
        }

        krsort($levels);

        foreach($levels as $level => $categories)
        {
            $parentLevel = $level - 1;

            if (isset($levels[$parentLevel]))
            {
                foreach($categories as $category)
                {
                    $_category = Avestique_Helper::useSystemData($category);
                    //if ($category['visibility'])
                    foreach($levels[$parentLevel] as $parent)
                    {
                        $_parent = Avestique_Helper::useSystemData($parent);
                        if ($_parent['left'] < $_category['left'] && $_parent['right'] > $_category['right'])
                        {
                            $levels[$parentLevel][$_parent['id_category']]['children'][$_category['id_category']] = Avestique_Helper::useCustomData($levels[$level][$_category['id_category']]);
                            break;
                        }
                    }
                }
            }
        }

        //$data = $levels[1];

        return $levels;
    }

    protected function getLeftKey($node)
    {
        if (!$this->leftKey)
            $this->leftKey = key(Avestique_Helper::useCustomData( array('left' => $node['left']) ));

        return $this->leftKey;
    }

    /**
     * Find All Children Nodes
     *
     * @param $idParentNode
     * @param bool $asTree show as a tree array
     */
    public function findChildrenNodes()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $children = $this->adapter->av_findNodesByLeftRight(
                array(Avestique_Helper::useCustomData( array('left' => $node['left'], 'right' => $node['right']) )),
                $getParent = false,
                $fullData = true
            );

            return $this->tree ? $this->getHierarchy($children) : $children;
        }

        return NULL;
    }

    public function findParents()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);
            $parents = $this->adapter->av_findNodesByLeftRight(
                array(Avestique_Helper::useCustomData( array('left' => $node['left'], 'right' => $node['right']) )),
                $getParent = true,
                $fullData = true
            );

            return $this->tree ? $this->getHierarchy($parents) : $parents;
        }

        return NULL;
    }

    public function findNeighbors()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $leftKey = $this->getLeftKey($node);

            $result = $this->adapter->av_findNeighbors(
                Avestique_Helper::useCustomData( array('parent_id' => $node['parent_id']) ),
                $leftKey
            );

            $data = array('left' => array(), 'right' => array());

            foreach($result as $item)
            {
                $systemFields = Avestique_Helper::useSystemData($item);

                if ($item[$leftKey] < $node['left'])
                    $data['left'][$systemFields['id_category']] = $item;
                else if ($item[$leftKey] > $node['right'])
                    $data['right'][$systemFields['id_category']] = $item;
            }

            if ($data)
                return $data;
        }

        return NULL;
    }

    public function findLeftNeighbor()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $result = $this->adapter->av_findNeighbors(
                Avestique_Helper::useCustomData( array('parent_id' => $node['parent_id']) ),
                $leftKey = $this->getLeftKey($node),
                Avestique_Helper::useCustomData( array('left' => $node['left']) ),
                self::FIND_LEFT,
                $limit = 1
            );

            if ($result)
                return current($result);
        }

        return NULL;
    }

    public function findLeftNeighbors()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $result = $this->adapter->av_findNeighbors(
                Avestique_Helper::useCustomData( array('parent_id' => $node['parent_id']) ),
                $leftKey = $this->getLeftKey($node),
                Avestique_Helper::useCustomData( array('right' => $node['right']) ),
                self::FIND_LEFT
            );

            return $result;
        }

        return NULL;
    }

    public function findRightNeighbor()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $result = $this->adapter->av_findNeighbors(
                Avestique_Helper::useCustomData( array('parent_id' => $node['parent_id']) ),
                $leftKey = $this->getLeftKey($node),
                Avestique_Helper::useCustomData( array('right' => $node['right']) ),
                self::FIND_RIGHT,
                $limit = 1
            );

            if ($result)
                return current($result);
        }

        return NULL;
    }

    public function findRightNeighbors()
    {
        if (count($this->nodes)==1)
        {
            $node = current($this->nodes);

            $result = $this->adapter->av_findNeighbors(
                Avestique_Helper::useCustomData( array('parent_id' => $node['parent_id']) ),
                $leftKey = $this->getLeftKey($node),
                Avestique_Helper::useCustomData( array('right' => $node['right']) ),
                self::FIND_RIGHT
            );

            return $result;
        }

        return NULL;
    }

    public function findNode()
    {
        $data = array();

        foreach($this->nodes as $node)
            $data[$node['id_category']] = Avestique_Helper::useCustomData($node);

        return $data;
    }

    public function getRootCategory()
    {
        return $this->getRootNode();
    }
}