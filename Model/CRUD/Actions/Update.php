<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Update.php
 */

class Avestique_Update_Node extends Avestique_CRUD_Actions_Abstract
{
    protected $idCategory = NULL;

    protected $data = array();

    public function init()
    {
        parent::init();

        $args = $this->getParams();

        $this->idCategory = (int) $args[0] ? (int) $args[0] : NULL;
        $this->data       = isset($args[1]) && is_array($args[1]) ? $args[1] : NULL;
    }

    public function run()
    {
        if ($this->idCategory && $node = $this->adapter->av_findNodesByID($this->idCategory))
        {
            if ($this->data)
            {
                $node = Avestique_Helper::useSystemData($node[0]);

                $reserved_fields = Avestique_Helper::registry('reserved_fields');
                $new_data        = Avestique_Helper::useSystemData($this->data);

                foreach($new_data as $field => $value)
                {
                    if (in_array($field, $reserved_fields))
                        unset($new_data[$field]);
                }

                $new_data = Avestique_Helper::useCustomData($new_data);

                if ($new_data)
                {
                    $this->adapter->av_startTransaction();

                    $this->success = Avestique_Event::raise('before_update', array('data' => $new_data, 'event' => $this));
                    $this->success = $this->success && $this->adapter->av_nodeSave($this->idCategory, $new_data);
                    $this->success = $this->success && Avestique_Event::raise('after_update', array('data' => $new_data, 'event' => $this));

                    if ($this->success)
                        $this->adapter->av_stopTransaction();
                    else
                        $this->adapter->av_rollBackTransaction();
                }
            }
        }
        else
        {
            $this->success = false;
            Avestique_Helper::registerMsg('Cannot find node ' . $this->idCategory . '.');
        }
    }
}