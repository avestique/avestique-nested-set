<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Move.php
 */

class Avestique_Move_Node extends Avestique_CRUD_Actions_Abstract
{
    protected $currentID;

    protected $parentID;

    protected $neighborID;

    protected $position;

    protected $_data = array();

    protected $_nodes  = array(
        'current'  => NULL,
        'parent'   => NUll,
        'neighbor' => NULL
    );

    public function init()
    {
        parent::init();

        $args = $this->getParams();

        $this->currentID  = isset($args[0]) && (int) $args[0]     ? $args[0] : NULL;
        $this->parentID   = isset($args[1]) && (int) $args[1]     ? $args[1] : NULL;
        $this->neighborID = isset($args[2]) && (int) $args[2]     ? $args[2] : NULL;
        $this->position   = isset($args[3]) && (int) $args[3]     ? $args[3] : 0;


    }

    public function getNodes()
    {
        return $this->_nodes;
    }

    private function getActualParentInfo()
    {
        $updatedParentNode = $this->adapter->av_findNodesByID(array($this->_nodes['parent']['id_category']));

        if (!$updatedParentNode)
        {
            Avestique_Helper::registerMsg("Cannot find parent category");
            return false;
        }

        $updatedParentNode = Avestique_Helper::useSystemData(current($updatedParentNode));

        if (!$updatedParentNode)
            $this->success = false;

        return $updatedParentNode;
    }

    /**
     * Remove offset of old node
     * update all 'right node' >= current 'right node' and left node > current 'right node' except children IDs
     */
    private function removeOffsetOldNode($children)
    {
        $leftCurrentNode = $this->_nodes['current']['left'];
        $rightCurrentNode = $this->_nodes['current']['right'];

        $result = $this->adapter->av_remove_offsetNode(
            Avestique_Helper::useCustomData(array( 'right' => $rightCurrentNode )),
            Avestique_Helper::useCustomData(array( 'left' => $rightCurrentNode )),
            $rightCurrentNode - $leftCurrentNode + 1,
            array('not in' => $children)
        );

        if (!$result)
            Avestique_Helper::registerMsg("Function removeOffsetOldNode cannot find any children");

        $this->success = $this->success && $result;
    }


    /**
     * Update right side of new parent node
     * update all 'right node' >= new parent 'right node' and left node > new parent 'right node' except children IDs
     */
    private function updateOffsetRightSide($branch)
    {
        $leftCurrentNode = $this->_nodes['current']['left'];
        $rightCurrentNode = $this->_nodes['current']['right'];

        if ($updatedParentNode = $this->getActualParentInfo())
        {
            if ($this->neighborID > 0 && $this->position == NestedSet::PLACE_AFTER &&
                ( $neighborNode = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData(array('id_category' => $this->neighborID))) )
                && ( $neighborNode = Avestique_Helper::useSystemData(current($neighborNode)) )
                && $neighborNode['parent_id'] == $updatedParentNode['id_category']
            ) // after
            {
                $neighborRight = $neighborNode['right'];

                $result = $this->adapter->av_offsetNodeMoveRight(
                    Avestique_Helper::useCustomData(array( 'right' => $neighborRight )),
                    Avestique_Helper::useCustomData(array( 'left' => $neighborRight )),
                    count($branch) * 2,
                    array('not in' => $branch)
                );

                if ($result)
                    $this->updateBranch($branch, $neighborRight + 1);
            }
            else if ($this->neighborID > 0 && $this->position == NestedSet::PLACE_BEFORE &&
                ( $neighborNode = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData(array('id_category' => $this->neighborID))) )
                && ( $neighborNode = Avestique_Helper::useSystemData(current($neighborNode)) )
                && $neighborNode['parent_id'] == $updatedParentNode['id_category']
            ) // before
            {
                $neighborLeft = $neighborNode['left'];
                $neighborRight = $neighborNode['right'];

                $result = $this->adapter->av_offsetNodeMoveLeft(
                    Avestique_Helper::useCustomData(array( 'right' => $neighborRight )),
                    Avestique_Helper::useCustomData(array( 'left' => $neighborLeft )),
                    count($branch) * 2,
                    array('not in' => $branch)
                );

                if ($result)
                    $this->updateBranch($branch, $neighborLeft );
            }
            else //last item in level
            {
                $rightParent = $updatedParentNode['right'];

                $result = $this->adapter->av_offsetNodeMoveEnd(
                    Avestique_Helper::useCustomData(array( 'right' => $rightParent )),
                    Avestique_Helper::useCustomData(array( 'left' => $rightParent )),
                    count($branch) * 2,
                    array('not in' => $branch)
                );

                if ($result)
                    $this->updateBranch($branch, $rightParent );
            }
        }

        if (!$result)
            Avestique_Helper::registerMsg("Function updateOffsetRightSide error");

        $this->success = $this->success && $result;
    }

   /*
    * Update left&right for branch which are moving
    * update all 'left node' > old current 'left node' and right node < old current 'right node' for children IDs
    */
    private function updateBranch($branch, $startIndex)
    {
        if ($this->success)
        {
            $leftCurrentNode = $this->_nodes['current']['left'];
            $rightCurrentNode = $this->_nodes['current']['right'];

            $result = $this->adapter->av_offsetNodeMove(
                Avestique_Helper::useCustomData(array( 'right' => $rightCurrentNode )),
                Avestique_Helper::useCustomData(array( 'left' => $leftCurrentNode )),
                $startIndex,
                $leftCurrentNode,
                $branch
            );

            if (!$result)
                Avestique_Helper::registerMsg("Function updateBranch cannot find any nodes");

            $this->success = $this->success && $result;
        }
    }

    public function run()
    {
        if ($this->currentID > 0 && $this->parentID > 0)
        {
            $nodes = $this->adapter->av_findNodesByID( array_merge(array( $this->currentID, $this->parentID ), ($this->neighborID ? array($this->neighborID) : array()) ) );

            foreach($nodes as $node)
            {
                $node = Avestique_Helper::useSystemData($node);

                if ($node['id_category'] == $this->currentID)
                    $this->_nodes['current'] = $node;
                else if ($node['id_category'] == $this->parentID)
                    $this->_nodes['parent'] = $node;
                else if ($node['id_category'] == $this->neighborID)
                    $this->_nodes['neighbor'] = $node;
            }

            if ($this->_nodes['parent'] && $this->_nodes['current'])
            {
                $children = $this->adapter->av_findNodesByLeftRight(array(Avestique_Helper::useCustomData( array('left' => $this->_nodes['current']['left'], 'right' => $this->_nodes['current']['right']) )));

                if (!in_array($this->_nodes['parent']['id_category'], $children))
                {
                    $this->adapter->av_startTransaction();

                    $branch = array_merge($children, array($this->_nodes['current']['id_category']));

                    $this->success = Avestique_Event::raise('before_move', array('branch' => $branch, 'event' => $this));

                    /* change parent */
                    if ($this->_nodes['current']['parent_id'] != $this->_nodes['parent']['id_category'])
                    {
                        $this->_data['parent_id'] = $this->_nodes['parent']['id_category'];
                        $this->_data['level'] = $this->_nodes['parent']['level'] + 1;
                    }


                    /* children doesn't have current NODE ID!!! */
                    $this->removeOffsetOldNode($children);

                    $this->updateOffsetRightSide($branch);

                    if ($this->success)
                    {
                        $this->success = $this->adapter->av_nodeSave($this->_nodes['current']['id_category'], Avestique_Helper::useCustomData($this->_data));

                        $nodes = $this->adapter->av_findNodesByID($branch);

                        foreach($nodes as $node)
                        {
                            $node = Avestique_Helper::useSystemData($node);

                            $parents = $this->adapter->av_findNodesByLeftRight(
                                array( Avestique_Helper::useCustomData( array('left' => $node['left'], 'right' => $node['right']) )),
                                $showParent = true
                            );

                            if (is_array($parents))
                            {
                                $data = array('path' => implode(NestedSet::PARENT_DELIMITER, $parents));

                                $this->success = $this->success && $this->adapter->av_nodeSave($node['id_category'], Avestique_Helper::useCustomData($data));
                            }
                        }

                        $this->success = Avestique_Event::raise('after_move', array('branch' => $nodes, 'event' => $this));
                    }

                    if ($this->success)
                        $this->adapter->av_stopTransaction();
                    else
                        $this->adapter->av_rollBackTransaction();
                }
                else
                    Avestique_Helper::registerMsg('Moving error: node cannot move to its child.');
            }
        }
    }
}