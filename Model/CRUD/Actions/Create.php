<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file CreateNode.php
 */

class Avestique_Create_Node extends Avestique_CRUD_Actions_Abstract
{
    protected $data = NULL;

    protected $parentID;

    protected $neighborID;

    protected $position;

    public function init()
    {
        parent::init();

        $args = $this->getParams();

        $this->data       = isset($args[0]) && is_array($args[0]) ? $args[0] : array();
        $this->parentID   = isset($args[1]) && (int) $args[1]     ? $args[1] : 0;
        $this->neighborID = isset($args[2]) && (int) $args[2]     ? $args[2] : NULL;
        $this->position   = isset($args[3]) && (int) $args[3]     ? $args[3] : 0;
        /* @param int $position -1 before | 1 after | 0 - the last item in the parent node */
    }

    public function getData()
    {
        return $this->data;
    }

    public function run()
    {
        if ($this->data = Avestique_Helper::useSystemData($this->data))
        {
            $rootNode = Avestique_Helper::useSystemData($this->getRootNode());

            /* if creation in the root */
            if ($this->parentID == 0)
            {
                $this->data['path']      = $rootNode['id_category'];
                $this->data['parent_id'] = $rootNode['id_category'];
                $this->data['level']     = 1;

                $parentNode = $rootNode;
            }
            else
            {
                $attributes = array(
                    'id_category' => $this->parentID
                );

                $parentNode = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData($attributes));

                if ($parentNode && $parentNode = Avestique_Helper::useSystemData(current($parentNode)))
                {
                    $this->data['parent_id'] = $parentNode['id_category'];
                    $this->data['level']     = $parentNode['level'] + 1;
                }
                else
                {
                    Avestique_Helper::registerMsg("Parent wasn't found!");
                }
            }

            $this->adapter->av_startTransaction();


            if ($parentNode)
            {
                if ($this->neighborID > 0 && $this->position == 1 &&
                        ( $neighborNode = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData(array('id_category' => $this->neighborID))) )
                            && ( $neighborNode = Avestique_Helper::useSystemData(current($neighborNode)) )
                                && $neighborNode['parent_id'] == $this->data['parent_id']
                ) // after
                {
                    $neighborRight = $neighborNode['right'];

                    $this->data['left'] = $neighborRight + 1;
                    $this->data['right'] = $neighborRight + 2;

                    // update where 'all right' > 'neighbor right' on 2
                    // update where 'all left' > 'neighbor right' on 2

                    $this->success = $this->success && Avestique_Event::raise('before_offset_node_right_create', array('neighborNode' => $neighborNode, 'event' => $this));
                    $offsetDone = $this->adapter->av_offsetNodeRight(
                        Avestique_Helper::useCustomData(array( 'right' => $neighborRight )),
                        Avestique_Helper::useCustomData(array( 'left' => $neighborRight ))
                    );
                    $this->success = $this->success && Avestique_Event::raise('after_offset_node_right_create', array('neighborNode' => $neighborNode, 'event' => $this));

                    $this->success = $this->success && $offsetDone;
                }
                else if ($this->neighborID > 0 && $this->position == -1 &&
                        ( $neighborNode = $this->adapter->av_findNodeByAttributes(Avestique_Helper::useCustomData(array('id_category' => $this->neighborID))) )
                            && ( $neighborNode = Avestique_Helper::useSystemData(current($neighborNode)) )
                                && $neighborNode['parent_id'] == $this->data['parent_id']
                ) // before
                {
                    $neighborLeft = $neighborNode['left'];
                    $neighborRight = $neighborNode['right'];

                    $this->data['left'] = $neighborLeft;
                    $this->data['right'] = $neighborRight;

                    // update where 'all right' >= 'neighbor right' on 2
                    // update where 'all left' >= 'neighbor left' on 2

                    $this->success = $this->success && Avestique_Event::raise('before_offset_node_left_create', array('neighborNode' => $neighborNode, 'event' => $this));
                    $offsetDone = $this->adapter->av_offsetNodeLeft(
                        Avestique_Helper::useCustomData(array( 'right' => $neighborRight )),
                        Avestique_Helper::useCustomData(array( 'left' => $neighborLeft ))
                    );
                    $this->success = $this->success && Avestique_Event::raise('after_offset_node_left_create', array('neighborNode' => $neighborNode, 'event' => $this));

                    $this->success = $this->success && $offsetDone;

                }
                else //last item in level
                {
                    $rightParent = $parentNode['right'];

                    $this->data['left'] = $rightParent;
                    $this->data['right'] = $rightParent + 1;

                    // update where 'all right' >= 'parent right' on 2
                    // update where 'all left' > 'parent right' on 2

                    $this->success = $this->success && Avestique_Event::raise('before_offset_node_end_create', array('parent_node' => $parentNode, 'event' => $this));
                    $offsetDone = $this->adapter->av_offsetNodeEnd(
                        Avestique_Helper::useCustomData(array( 'right' => $rightParent )),
                        Avestique_Helper::useCustomData(array( 'left' => $rightParent ))
                    );
                    $this->success = $this->success && Avestique_Event::raise('after_offset_node_end_create', array('parent_node' => $parentNode, 'event' => $this));

                    $this->success = $this->success && $offsetDone;
                }

                /* \/ node creation */
                if ($offsetDone)
                {
                    if (!isset($this->data['path']))
                    {
                        $parents = $this->adapter->av_findNodesByLeftRight(
                            array( Avestique_Helper::useCustomData( array('left' => $this->data['left'], 'right' => $this->data['right']) )),
                            $showParent = true
                        );

                        if (is_array($parents))
                            $this->data['path'] = implode(NestedSet::PARENT_DELIMITER, $parents);
                    }

                    $this->success = $this->success && Avestique_Event::raise('before_create', array('event' => $this));
                    $created = $this->adapter->av_createNode(Avestique_Helper::useCustomData($this->data));
                    $this->success = $this->success && $created;
                    $this->success = $this->success && Avestique_Event::raise('after_create', array('event' => $this));

                    if (!$created)
                    {
                        Avestique_Helper::registerMsg("Category creation error.");
                    }
                }
                else
                    Avestique_Helper::registerMsg("Left/Right update error.");
            } // if parentNode

            /* /\ node creation */

            if (isset($this->success) && $this->success)
                $this->adapter->av_stopTransaction();
            else
                $this->adapter->av_rollBackTransaction();

        }
        else
        {
            Avestique_Helper::registerMsg("Data error.");
        }
    }
}