<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @company     Avestique Developer
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Anton Sannikov http://avestique.ru/
 * @file Remove.php
 */

class Avestique_Remove_Node extends Avestique_CRUD_Actions_Abstract
{
    protected $idCategory = array();

    public function init()
    {
        parent::init();

        $args = $this->getParams();

        if (isset($args[0]))
        {
            if (is_array($args[0]))
                $this->idCategory = $args[0];
            else
                $this->idCategory = (int) $args[0] ? array($args[0]) : NULL;
        }
    }

    public function run()
    {
        if ($this->idCategory)
        {
            $nodesToRemoveData = $this->adapter->av_findNodesByID($this->idCategory);

            $children = array();

            foreach($nodesToRemoveData as $node)
            {
                $node = Avestique_Helper::useSystemData($node);
                $children[] = Avestique_Helper::useCustomData( array('left' => $node['left'], 'right' => $node['right']) );
            }

            $childrenNodesToRemove = $this->adapter->av_findNodesByLeftRight($children);

            $nodesToRemove = array();

            foreach($nodesToRemoveData as $i => $node)
            {
                $node = Avestique_Helper::useSystemData($node);
                $nodesToRemove[$i] = $node['id_category'];
            }

            $nodesToRemove = array_unique(array_merge($nodesToRemove, $childrenNodesToRemove));

            $this->adapter->av_startTransaction();

            $this->success = Avestique_Event::raise('before_remove', array('nodes_to_remove' => $nodesToRemove, 'event' => $this));
            $this->success = $this->success && $this->adapter->av_removeNodes($nodesToRemove);

            /*
            * Remove offset of old node
            * update all 'right node' >= current 'right node' and left node > current 'right node' except children IDs
            */
            if ($this->adapter->av_GetCount() > 0)
                foreach($nodesToRemoveData as $i => $node)
                {
                    $node = Avestique_Helper::useSystemData($node);

                    $leftCurrentNode = $node['left'];
                    $rightCurrentNode = $node['right'];

                    $this->success =  $this->success && $this->adapter->av_remove_offsetNode(
                        Avestique_Helper::useCustomData(array( 'right' => $rightCurrentNode )),
                        Avestique_Helper::useCustomData(array( 'left' => $leftCurrentNode )),
                        $rightCurrentNode - $leftCurrentNode + 1
                    );
                }

            $this->success = $this->success && Avestique_Event::raise('after_remove', array('nodes_to_remove' => $nodesToRemove, 'event' => $this));

            if ($this->success)
                $this->adapter->av_stopTransaction();
            else
                $this->adapter->av_rollBackTransaction();
        }
    }
}