<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Abstract.php
 */

include_once realpath(__DIR__) . '/Interface.php';

abstract class Avestique_Crud_Abstract implements Avestique_Crud_Interface
{
    protected $adapter = NULL;

    public function getAdapter()
    {
        return $this->adapter;
    }

    public function createRoot()
    {
        $this->adapter;
    }

    /* actions */
    abstract public function createNode($data, $parentID = 0, $idNeighbor = NULL, $position = 0);

    abstract public function removeNode($idNode);

    abstract public function moveNode($node, $newParentID, $idNeighbor = NULL, $position = 0);

    abstract public function copyNode($node, $parentID, $idNeighbor = NULL, $position = 0);

    abstract public function editNode($id, $node);

    /* fetching */
    abstract public function findNode($idNode);

    abstract public function findChildrenNodes($idParentNode, $asTree = false);

    abstract public function findParents($idNode, $asTree = false);

    abstract public function findLeftNeighbor($idNode);

    abstract public function findLeftNeighbors($idNode);

    abstract public function findRightNeighbor($idNode);

    abstract public function findRightNeighbors($idNode);
}