<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file save.php
 */

class Avestique_CRUD_Adapter_Yii_Behavior_Save extends CActiveRecordBehavior
{
    function av_node_create($data)
    {
        //$customData = Avestique_Helper::registry('custom_required_fields');

        $model = new Avestique_Crud_Adapter_Yii();

        foreach($data as $field => $value)
        {
            $model->$field = $value;
        }

        $saved = $model->save();

        if (!$saved)
        {
            $errors = $model->getErrors();

            foreach($errors as $error)
                Avestique_Helper::registerMsg($error[0]);
        }

        return $saved;
    }

    function av_node_save($id, $data)
    {
        $node = Avestique_Crud_Adapter_Yii::model()->findByPk($id);

        if ($node)
        {
            foreach($data as $field => $value)
            {
                if ( $field!== Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey)
                {
                    $node->$field = $value;
                }
            }

            $saved = $node->save();

            if (!$saved)
            {
                $errors = $node->getErrors();

                foreach($errors as $error)
                    Avestique_Helper::registerMsg($error[0]);
            }

            return $saved;
        }

        return false;
    }

    function av_offset_node_end($rightField, $leftField)
    {
        if (is_array($rightField) && count($rightField) == 1 &&
                is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);
            $rightValue = current($rightField);

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + 2 ") ),
                " `$rightKey` >= :$rightKey ",
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + 2 ") ),
                " `$leftKey` >= :$leftKey ",
                array(":$leftKey" => $rightValue)
            ) || $updated;

            return $updated;
        }
    }

    function av_offset_node_move_end($rightField, $leftField, $offset = 2, $condition = array())
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);
            $rightValue = current($rightField);

            $where = '';

            if ($condition && in_array(key($condition), array('in','not in')) && $IDs = current($condition))
            {
                $where = " AND " . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . " " . key($condition) . " (" . implode(',', $IDs) . ")";
            }

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + $offset ") ),
                " `$rightKey` >= :$rightKey " . $where,
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + $offset ") ),
                " `$leftKey` >= :$leftKey " . $where,
                array(":$leftKey" => $rightValue)
            ) || $updated;

            return $updated;
        }
    }

    function av_offset_node_left($rightField, $leftField)
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);

            $rightValue = current($rightField);
            $leftValue = current($leftField);

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + 2 ") ),
                " `$rightKey` >= :$rightKey ",
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + 2 ") ),
                " `$leftKey` >= :$leftKey ",
                array(":$leftKey" => $leftValue)
            ) || $updated;

            return $updated;
        }
    }

    function av_offset_node_right($rightField, $leftField)
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);
            $rightValue = current($rightField);

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + 2 ") ),
                " `$rightKey` > :$rightKey ",
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + 2 ") ),
                " `$leftKey` > :$leftKey ",
                array(":$leftKey" => $rightValue)
            ) || $updated;

            return $updated;
        }
    }

    function av_offset_node_move($rightField, $leftField, $startFrom, $currentLeftMin, $IDs)
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);

            $rightValue = current($rightField);
            $leftValue = current($leftField);

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array(
                    "$leftKey"  => new CDbExpression( "`$leftKey` - $currentLeftMin + $startFrom "),// + 1
                    "$rightKey" => new CDbExpression( "`$rightKey` - $currentLeftMin + $startFrom ") // + 1
                ),
                " `$leftKey` >= :$leftKey AND `$rightKey` <= :$rightKey AND `" . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . "` IN (" . implode(',', $IDs) . ')',
                array(":$leftKey" => $leftValue, ":$rightKey" => $rightValue)
            );

            return $updated;
        }
    }

    function av_remove_offset_node($rightField, $leftField, $offset, $exclude = array())
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);

            $rightValue = current($rightField);

            $where = '';

            if ($exclude && in_array(key($exclude), array('in','not in')) && $IDs = current($exclude))
            {
                $where = " AND " . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . " " . key($exclude) . " (" . implode(',', current($IDs)) . ")";
            }

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` - $offset ") ),
                " `$rightKey` > :$rightKey " . $where,
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` - $offset ") ),
                " `$leftKey` > :$leftKey " . $where,
                array(":$leftKey" => $rightValue)
            ) || $updated;


            return $updated;
        }
    }

    function av_offset_node_move_right($rightField, $leftField, $offset = 2, $condition = array())
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);
            $rightValue = current($rightField);

            $where = '';

            if ($condition && in_array(key($condition), array('in','not in')) && $IDs = current($condition))
            {
                $where = " AND " . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . " " . key($condition) . " (" . implode(',', $IDs) . ")";
            }

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + $offset ") ),
                " `$rightKey` > :$rightKey " . $where,
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + $offset ") ),
                " `$leftKey` > :$leftKey " . $where,
                array(":$leftKey" => $rightValue)
            ) || $updated;

            return $updated;
        }
    }

    function av_offset_node_move_left($rightField, $leftField, $offset = 2, $condition = array())
    {
        if (is_array($rightField) && count($rightField) == 1 &&
            is_array($leftField) && count($leftField) == 1)
        {
            $leftKey = key($leftField);
            $rightKey = key($rightField);
            $leftValue = current($leftField);
            $rightValue = current($rightField);


            $where = '';

            if ($condition && in_array(key($condition), array('in','not in')) && $IDs = current($condition))
            {
                $where = " AND " . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . " " . key($condition) . " (" . implode(',', $IDs) . ")";
            }

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$rightKey" => new CDbExpression( "`$rightKey` + $offset ") ),
                " `$rightKey` >= :$rightKey " . $where,
                array(":$rightKey" => $rightValue)
            );

            $updated = Avestique_Crud_Adapter_Yii::model()->updateAll(
                array("$leftKey" => new CDbExpression( "`$leftKey` + $offset ") ),
                " `$leftKey` >= :$leftKey " . $where,
                array(":$leftKey" => $leftValue)
            ) || $updated;

            return $updated;
        }
    }
}