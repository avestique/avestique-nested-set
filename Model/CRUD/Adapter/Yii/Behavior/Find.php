<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Find.php
 */

class Avestique_CRUD_Adapter_Yii_Behavior_Find extends CActiveRecordBehavior
{
    public function av_find_node_by_attributes($data)
    {
        $criteria = new CDbCriteria();

        foreach($data as $field => $value)
            $criteria->condition .= ( $criteria->condition ? ' AND ' : '') . "`$field` = :$field";

        $params = array();

        foreach($data as $field => $value)
            $params[":$field"] = $value;

        $criteria->params = $params;

        $result = Avestique_Crud_Adapter_Yii::model()->findAll($criteria);

        $nodes = array();

        if ($result)
            foreach($result as $node)
            {
                $id = $node->getTableSchema()->primaryKey;
                $nodes[$node->$id] = $node->getAttributes();
            }

        return $nodes;
    }

    public function av_find_node_by_id($ids)
    {
        $command = Yii::app()->db->createCommand();

        $result = $command->select("*")
                          ->from( Avestique_Crud_Adapter_Yii::model()->tableName() )
                          ->where( array('in', Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey, $ids) )
                          ->queryAll();

        return $result;
    }

    public function av_find_nodes_by_left_right($children, $parent = false, $fullData = false)
    {
        if ($children)
        {
            $conditions = array('or');

            foreach($children as $node)
            {
                $leftKey =  key($node);
                $leftValue =  current($node);
                $rightValue =  end($node);
                $rightKey =  key($node);

                $conditions[] = "`" . $leftKey . "` " . ($parent ? '< ' : '> ') . $leftValue . " AND " . "`" . $rightKey . "` " . ($parent ? '> ' : '< ') . $rightValue;
            }

            $command = Yii::app()->db->createCommand();

            $result = $command->select($fullData ? '*' : Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey)
                                ->from( Avestique_Crud_Adapter_Yii::model()->tableName() )
                                ->where( $conditions )
                                ->order($leftKey);
            if ($fullData)
                return $result->queryAll();
            else
                return $result->queryColumn();
        }

        return array();
    }

    public function av_find_neighbors($parent, $leftKey, $field = array(), $direction = 0, $limit = 0)
    {
        if (is_array($parent))
        {
            $query = Yii::app()->db->createCommand()
                           ->select("*")
                           ->from( Avestique_Crud_Adapter_Yii::model()->tableName() )
                           ->where("`" . key($parent) . "` =:" . key($parent) , array(":" . key($parent) => current($parent)));

            if (is_array($field) && $direction < 0)
            {
                $query->andWhere("`" . key($field) . "` < :" . key($field) , array(":" . key($field) => current($field)));
            }
            else if (is_array($field) && $direction > 0)
            {
                $query->andWhere("`" . key($field) . "` > :" . key($field) , array(":" . key($field) => current($field)));
            }

            if ($limit > 0)
                $query->limit($limit);

            $query->order($leftKey);

            return $query->queryAll();
        }

        return array();
    }
}
