<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Yii.php
 */

include_once realpath(__DIR__) . '/Interface.php';
include_once realpath(__DIR__) . '/Yii/Behavior/Save.php';
include_once realpath(__DIR__) . '/Yii/Behavior/Find.php';

class Avestique_Crud_Adapter_Yii extends CActiveRecord implements Avestique_Crud_Adapter_Interface
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MCategory the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function checkTableStructure()
    {
        Avestique_Helper::checkTableFields($this->getTableInfo());
    }

    public function init()
    {
        parent::init();

        $this->checkTableStructure();

        $this->attachBehavior('av_save', new Avestique_CRUD_Adapter_Yii_Behavior_Save());
        $this->attachBehavior('av_find', new Avestique_CRUD_Adapter_Yii_Behavior_Find());
    }

    public function getTableInfo()
    {
        $tableInfo = array();

        foreach($this->getTableSchema()->getColumnNames() as $columnName)
        {
            $tableInfo[$columnName]['name'] = $this->getTableSchema()->getColumn($columnName)->name;
            $tableInfo[$columnName]['allow_null'] = $this->getTableSchema()->getColumn($columnName)->allowNull;
            $tableInfo[$columnName]['type'] = $this->getTableSchema()->getColumn($columnName)->type;
            $tableInfo[$columnName]['default'] = $this->getTableSchema()->getColumn($columnName)->defaultValue;
            $tableInfo[$columnName]['size'] = $this->getTableSchema()->getColumn($columnName)->size;
            $tableInfo[$columnName]['primary'] = $this->getTableSchema()->getColumn($columnName)->isPrimaryKey;
        }

        return $tableInfo;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Avestique_Helper::registry(Avestique_Crud::TABLE_NAME_KEY);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        if ($options = Avestique_Helper::registry('adapter_options') && isset($options['rules']))
        {
            return $options['rules'];
        }

        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        if ($options = Avestique_Helper::registry('adapter_options') && isset($options['labels']))
        {
            return $options['labels'];
        }

        return array();
    }

    protected $transaction;

    public function av_nodeSave($id, $data)
    {
        return $this->av_node_save($id, $data);
    }

    /* interfaces */

    public function av_startTransaction()
    {
        $this->transaction = Yii::app()->db->beginTransaction();
    }

    public function av_rollBackTransaction()
    {
        if (isset($this->transaction))
            $this->transaction->rollback();
    }

    public function av_stopTransaction()
    {
        if (isset($this->transaction))
            $this->transaction->commit();
    }

    public function av_GetCount()
    {
        return self::model()->count();
    }

    public function av_createNode($data)
    {
        return $this->av_node_create($data);
    }

    public function av_findNodeByAttributes($data)
    {
        return $this->av_find_node_by_attributes($data);
    }

    /* find */
    public function av_findNodesByID($IDs)
    {
        return $this->av_find_node_by_id($IDs);
    }

    public function av_findNodesByLeftRight($nodes, $parent = false, $fullData = false)
    {
        return $this->av_find_nodes_by_left_right($nodes, $parent, $fullData);
    }

    public function av_findNeighbors($parent, $leftKey, $field = array(), $direction = 0, $limit = 0)
    {
        return $this->av_find_neighbors($parent, $leftKey, $field, $direction, $limit);
    }

    /* move */
    public function av_offsetNodeEnd($rightField, $leftField)
    {
        return $this->av_offset_node_end($rightField, $leftField);
    }

    public function av_offsetNodeMoveEnd($rightField, $leftField, $offset = 2, $condition)
    {
        return $this->av_offset_node_move_end($rightField, $leftField, $offset, $condition);
    }

    public function av_offsetNodeLeft($rightField, $leftField)
    {
        return $this->av_offset_node_left($rightField, $leftField);
    }

    public function av_offsetNodeMoveLeft($rightField, $leftField, $offset = 2, $condition)
    {
        return $this->av_offset_node_move_left($rightField, $leftField, $offset, $condition);
    }

    public function av_offsetNodeRight($rightField, $leftField)
    {
        return $this->av_offset_node_right($rightField, $leftField);
    }

    public function av_offsetNodeMoveRight($rightField, $leftField, $offset = 2, $condition)
    {
        return $this->av_offset_node_move_right($rightField, $leftField, $offset, $condition);
    }

    /* remove */
    public function av_removeNodes($IDs)
    {
        return self::model()->deleteAll("`" . Avestique_Crud_Adapter_Yii::model()->getTableSchema()->primaryKey . "` IN (" . implode(',', $IDs) . ')');
    }

    public function av_remove_offsetNode($rightField, $leftField, $offset, $exclude = array())
    {
        return $this->av_remove_offset_node($rightField, $leftField, $offset, $exclude);
    }

    /* update */
    public function av_offsetNodeMove($rightField, $leftField, $startFrom, $currentLeftMin, $IDs)
    {
        return $this->av_offset_node_move($rightField, $leftField, $startFrom, $currentLeftMin, $IDs);
    }
}