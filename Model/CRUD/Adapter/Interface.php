<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Interface.php
 */

interface Avestique_Crud_Adapter_Interface
{
    public function getTableInfo();

    public function checkTableStructure();

    /**
     * get count of records in the table
     *
     * @abstract
     * @return mixed
     */
    public function av_GetCount();

    public function av_createNode($data);

    public function av_nodeSave($id, $data);

    /* transaction */

    public function av_startTransaction();

    public function av_rollBackTransaction();

    public function av_stopTransaction();

    /* find */
    public function av_findNodeByAttributes($data);

    /**
     * @param array $IDs
     */
    public function av_findNodesByID($IDs);

    public function av_findNodesByLeftRight($nodes, $parent = false);

    /* update */
    public function av_offsetNodeEnd($rightField, $leftField);

    /* $condition = array('in|not in' => array(1,2,...) */
    public function av_offsetNodeMoveEnd($rightField, $leftField, $offset = 2, $condition);

    public function av_offsetNodeLeft($rightField, $leftField);

    /* $condition = array('in|not in' => array(1,2,...) */
    public function av_offsetNodeMoveLeft($rightField, $leftField, $offset = 2, $condition);

    public function av_offsetNodeRight($rightField, $leftField);

    /* $condition = array('in|not in' => array(1,2,...) */
    public function av_offsetNodeMoveRight($rightField, $leftField, $offset = 2, $condition);

    /**
     * @param array $IDs
     */
    public function av_offsetNodeMove($rightField, $leftField, $startFrom, $currentLeftMin, $IDs);

    /* remove */
    /**
     * @param array $IDs
     */
    public function av_removeNodes($IDs);

    public function av_remove_offsetNode($rightField, $leftField, $offset, $exclude = array());
}