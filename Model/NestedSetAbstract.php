<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file NestedSetAbstract.php
 */

abstract class Avestique_NestedSetAbstract
{
    protected $_tableName = NULL;

    protected $crudModel = NULL;

    protected $_adapter = NULL;

    /**
     * reserved fields
     *
     * @var array
     */
    protected $_requiredFields = array(
        'id_category' => 'id_category',
        'name'        => 'name',
        'alias'       => 'alias',
        'level'       => 'level',
        'path'        => 'path',
        'left'        => 'left',
        'right'       => 'right',
        'parent_id'   => 'parent_id'
    );

    protected $_reservedFields = array(
        'id_category', 'level', 'path', 'left', 'right', 'parent_id'
    );

    /**
     * $options['adapter'] = 'Yii';
     * $options['adapter'] = array('Yii', array(...));
     *
     * $options['table_name'] = '';
     *
     * $options['unique'] = array('custom_field')
     *
     * $options['fields'] = array(
     *    'id_category' => 'id_category',
     *     'name'        => 'name',
     *     'alias'       => 'alias',
     *     'level'       => 'level',
     *     'path'        => 'path',
     *     'left'        => 'left',
     *     'right'       => 'right',
     *     'parent_id'   => 'parent_id'
     *     ....
     *     additional fields
     * );
     *
     *
     * @param array $options
     */
    public function setOptions($options)
    {
        if ($options && is_array($options))
        {
            if (isset($options['table_name']))
                $this->_tableName = (string) $options['table_name'];

            $this->setAdapterOptions($options);

            if (isset($options['fields']))
                $this->_requiredFields = array_merge($this->_requiredFields, array_uintersect_assoc($options['fields'], $this->_requiredFields, function(){}));

            Avestique_Helper::register('custom_required_fields', $this->_requiredFields);
            Avestique_Helper::register('reserved_fields',        $this->_reservedFields);
        }

        //$this->crudModel

        return $this;
    }

    protected function setAdapterOptions($options)
    {
        if (isset($options['adapter']))
        {
            if (is_array($options['adapter']))
            {
                $this->_adapter = (string) $options['adapter'][0];

                if (isset($options['adapter'][1]) && is_array($options['adapter'][1]))
                {
                    Avestique_Helper::register('adapter_options', $options['adapter'][1]);
                }
            }
            else
                $this->_adapter = (string) $options['adapter'];
        }
    }

    protected function afterInit()
    {
        if ($this->crudModel instanceof Avestique_Crud_Interface)
        {
            $adapter = $this->crudModel->getAdapter();

            if ($adapter instanceof Avestique_Crud_Adapter_Interface)
            {

            }
        }
    }

    public function init()
    {
        if ($this->crudModel instanceof Avestique_Crud_Interface && $this->_tableName && $this->_adapter)
        {
            $adapter = $this->crudModel->getAdapter();

            if (!$adapter)
            {
                $this->crudModel->init($this->_tableName, $this->_adapter);

                $adapter = $this->crudModel->getAdapter();
            }

            if ($diff = array_diff( $this->_requiredFields, array_keys($adapter->getTableInfo()) ))
            {
                throw new Exception("Required fields are absent in " . $this->_tableName . ": " . implode(', ', $diff) );
            }
        }

        return $this;
    }

    public function getCRUDModel()
    {
        return $this->crudModel;
    }

    public function getErrors()
    {
        return Avestique_Helper::getMsg();
    }

    /* actions */
    abstract public function createNode($data, $parentID = 0, $idNeighbor = NULL, $position = 0);

    abstract public function removeNode($idNode);

    abstract public function moveNode($node, $newParentID, $idNeighbor = NULL, $position = 0);

    abstract public function copyNode($node, $parentID, $idNeighbor = NULL, $position = 0);

    abstract public function editNode($id, $node);

    /* fetching */
    abstract public function findNode($idNode);

    abstract public function findChildrenNodes($idParentNode, $asTree = false);

    abstract public function findParents($idNode, $asTree = false);

    abstract public function findLeftNeighbor($idNode);

    abstract public function findLeftNeighbors($idNode);

    abstract public function findRightNeighbor($idNode);

    abstract public function findRightNeighbors($idNode);
}