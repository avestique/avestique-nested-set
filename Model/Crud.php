<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_NestedSet
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @author Anton Sannikov http://avestique.ru/
 * @file Crud.php
 */

include_once realpath(__DIR__) . "/CRUD/Abstract.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Abstract.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Create.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Remove.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Move.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Update.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Copy.php";
include_once realpath(__DIR__) . "/CRUD/Actions/Find.php";


class Avestique_Crud extends  Avestique_Crud_Abstract
{
    const TABLE_NAME_KEY = 'table_name';

     function __construct($options = array())
     {
         if (isset($options['adapter']) && isset($options['table_name']))
         {
            $this->init($options['table_name'], $options['adapter']);
         }
     }

     public function init($table_name = '', $adapter = '')
     {
         if ( ! $this->getAdapter() instanceof Avestique_Crud_Adapter_Interface)
         {
             $adapterPath = realpath(__DIR__) . '/CRUD/Adapter/';

             try
             {
                 include_once $adapterPath . $adapter . '.php';

                 Avestique_Helper::register(self::TABLE_NAME_KEY, $table_name);
                 $classAdapterName = "Avestique_Crud_Adapter_" . $adapter;
                 $this->adapter = new $classAdapterName();
             }
             catch (Exception $e)
             {
                 throw new Exception("Adapter error: " . $e->getMessage());
             }
         }
     }

    /**
     * Create new node.
     *
     * @param array $data
     * @param int $parentID If parentID = 0 its a root directory.
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */
    public function createNode($data, $parentID = 0, $idNeighbor = NULL, $position = 0)
    {
        $nodeObj = new Avestique_Create_Node($this->getAdapter(), $data, $parentID, $idNeighbor, $position);

    }

    /**
     * Remove node
     *
     * @param $idNode
     */
    public function removeNode($idNode)
    {
        $nodeObj = new Avestique_Remove_Node($this->getAdapter(), $idNode);
    }

    /**
     * Move node.
     *
     * @param int $currentID
     * @param int $newParentID move to new parent
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */

    public function moveNode($currentID, $newParentID, $idNeighbor = NULL, $position = 0)
    {
        $nodeObj = new Avestique_Move_Node($this->getAdapter(), $currentID, $newParentID, $idNeighbor, $position);
    }

    /**
     * Copy node.
     *
     * @param array $node
     * @param int $newParentID move to new parent
     * @param $idNeighbor set node after|before $idNeighbor
     * @param int $position -1 before | 1 after | 0 - the last item in the parent node
     */
    public function copyNode($node, $parentID, $idNeighbor = NULL, $position = 0)
    {

    }

    /**
     * Edit node
     *
     * @param $currentID
     * @param array $data
     *
     */
    public function editNode($currentID, $data)
    {
        $nodeObj = new Avestique_Update_Node($this->getAdapter(), $currentID, $data);
        return $nodeObj->getResult();
    }

    /**
     * Find node or nodes
     *
     * @param mixed $idNode array|integer
     */
    public function findNode($idNode)
    {
        $nodeObj = new Avestique_Update_Node($this->getAdapter(), $idNode);
        return $nodeObj->findNode();
    }

    /**
     * Find All Children Nodes
     *
     * @param $idParentNode
     * @param bool $asTree show as a tree array
     */
    public function findChildrenNodes($idParentNode, $asTree = false)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idParentNode, $asTree);
        return $nodeObj->findChildrenNodes();
    }

    public function findParents($idNode, $asTree = false)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode, $asTree);
        return $nodeObj->findParents();
    }

    public function findLeftNeighbor($idNode)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode);
        return $nodeObj->findLeftNeighbor();
    }

    public function findLeftNeighbors($idNode)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode);
        return $nodeObj->findLeftNeighbors();
    }

    public function findRightNeighbor($idNode)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode);
        return $nodeObj->findRightNeighbor();
    }

    public function findRightNeighbors($idNode)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode);
        return $nodeObj->findRightNeighbors();
    }

    public function findNeighbors($idNode)
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter(), $idNode);
        return $nodeObj->findNeighbors();
    }

    public function getRootCategory()
    {
        $nodeObj = new Avestique_Find_Node($this->getAdapter());
        return $nodeObj->getRootCategory();
    }
}